<http://www.w3.org/TR/xhtml1/DTD/ xhtml1-transitional.dtd"> 
 <html xmlns="http://www.w3.org/1999/ xhtml" xml:lang="en" lang="en"> 
  <head> 
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
 <title>Cars</title> 
  <style type="text/css" title="text/css"> 
 label { 
  font-weight: bold;
  color: indigo;  
  } 
  body {
	  background-color: whitesmoke;
  }
  </style> 
  <?php 
  include 'carsconnect.php';
  ?>
  </head> 
  <body> 
 <form action="server_put.php" 
method="post">  
 <fieldset><legend><h3>Customer Information</h3></legend> 
 <label>First: <input type="text" name="first" size="30" maxlength= "60" /></label><br />  
<br /><label>Last: <input type="text" name="last" size="30" maxlength="60" /></label><br /> 
<br /><label>Email: <input type="text" name="email" size="30" maxlength="100" /></label><br />
<br /><label>Phone Number: <input type="text" name="phone_num" size="15" maxlength="15" /></label>
 </fieldset> 
 <fieldset><legend><h3>Vehicle Information</h3></legend>
<label>Year: <label/>
  <?php
  // Sets the top option to be the current year. 
  $currently_selected = date('Y'); 
  // Year to start available options at
  $earliest_year = 1900; 
  // Latest year in the range
  $latest_year = date('2050'); 

  print '<select name="year">';
  // Loops over each int[year] from current year, back to the $earliest_year [1900]
  foreach ( range( $latest_year, $earliest_year ) as $i ) {
    // Prints the option with the next year in range.
    print '<option value="'.$i.'"'.($i === $currently_selected ? ' selected="selected"' : '').'>'.$i.'</option>';
  }
  print '</select>';
  ?>
  <br />  
<br /><label>Make: <input type="text" name="make" size="30" maxlength="150" /></label>
<br /><p><label>Model: <input type="text" name="model" size="30" maxlength="150" /></label><br />
<br /><label>VIN: <input type="text" name="vin" size="20" maxlength="50" /></label><br />
</fieldset>
<fieldset><legend><h3>Part Information</h3></legend> 
<label>Part Number: <input type="text" name="part_num" size="20" maxlength="150" /></label><br />
<br /><label>Part Name: <input type="text" name="part_name" size="30" maxlength="150" /></label><br />
<br /><label>Part Cost: <input type="text" name="part_cost" size="10" maxlength="10" /></label><br />
</fieldset>
<br />
<label>Total Cost: <input type="text" name="total_cost" size="10" maxlength="20" /></label>
 <h3>Comments: </h3>
 <textarea name="comment" rows="4" cols="60"> </textarea></label>
<input type= "submit" name="submit"  value= "Submit" />  
 </form>  
 </body>
 </html> 